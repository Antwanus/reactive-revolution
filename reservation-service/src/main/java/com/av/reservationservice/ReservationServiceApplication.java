package com.av.reservationservice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.function.Consumer;

@SpringBootApplication
public class ReservationServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(ReservationServiceApplication.class, args);
	}
}

@Document @Data @AllArgsConstructor @NoArgsConstructor
class Reservation {
	@Id
	private String id;
	private String name;
}
interface ReservationRepository extends ReactiveCrudRepository<Reservation, String> { }
@Component @RequiredArgsConstructor @Log4j2
class SampleDataInitializer {
	private final ReservationRepository reservationRepo;
	@EventListener(ApplicationReadyEvent.class)
	public void ready() {
//		Flux<String> names = Flux.just("Aaaa", "Bbbb", "Cccc", "Dddd", "Eeee", "Ffff", "Gggg", "Hhhh");
//		Flux<Reservation> reservations = names.map(name -> new Reservation(null, name));
////		Flux<Mono<Reservation>> reservationMonoFlux = reservations.map(this.reservationRepo::save);
//		Flux<Reservation> saved = reservations.flatMap(reservationRepo::save);
		var saved = Flux	.just("Aaaa", "Bbbb", "Cccc", "Dddd", "Eeee", "Ffff", "Gggg", "Hhhh")
											.map(name -> new Reservation(null, name))
											.flatMap(reservationRepo::save);

		this.reservationRepo	.deleteAll()
								.thenMany(saved)
								.thenMany(this.reservationRepo.findAll()).subscribe(log::info);

//		saved.subscribe(log::info);

	}
}
